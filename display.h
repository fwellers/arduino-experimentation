#include <avr/io.h>
#include <util/delay.h>
#include <stdint.h>

#define RS PORTD4
#define E  PORTD5

#define D0 PORTD6
#define D1 PORTD7
#define D2 PORTB0
#define D3 PORTB1
#define D4 PORTB2
#define D5 PORTB3
#define D6 PORTB4
#define D7 PORTB5

#define OPCODE_ON 0x0F
#define OPCODE_OFF 0x08
#define OPCODE_ENTRY_MODE_SET 0x06
#define OPCODE_CLEAR 0x01
#define OPCODE_FUNCTION_SET 0x38

#define MODE_4BIT 0
#define MODE_8BIT 1

#define INPUT_DATA 1
#define INPUT_INSTRUCTION 0

void init();

void transfer_payload(uint8_t inputtype, uint8_t transfermode);
void start_display();
int set_pin(uint8_t value, uint8_t pin, int port);