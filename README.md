# Experimenting with Arduino

Just screwing around with hardware components :)

## Sub-projects

| **Branch** | **Description** | **Components** | **Build** **Design** |
| ------ | ------ | ------ | ----- |
| led-interrupts | Toggling LEDs by mapping <BR> an interrupt source to a button | <ul><li>1x Arduino</li><li>1x LED // ???Ω Resisitor </li><li>1x Button</ul> | N/A |
| lcd-display | Configuring and writing chars <BR> to an LCD display | <ul><li>1x Arduino</li><li>1x LCM1602C LCD display</li><li>1x Potentiometer</li><li>1x 1K Ω Resistors </ul> | N/A |
| spi | Communication between two <BR> arduinos over their SPI interfaces |  <ul><li>2x Arduino</ul> | N/A |

## Links

|**Interesting pointers**| **Association** |
| ----- | ----- |
|[Building and flashing](https://balau82.wordpress.com/2011/03/29/programming-arduino-uno-in-pure-c/)| Basics |
|[Writing to GPIOs under AVR architecture](https://www.elecrom.com/avr-tutorial-2-avr-input-output/)| Basics |
|[Tutorial for interrupt handling under AVR architecutre](https://microchipdeveloper.com/8avr:pin-change-interrupts)| Interrupts |
|[Using VSCode with Arduino](https://www.makeuseof.com/tag/better-arduino-coding-vs-code-platformio/)| IDE |
|[Datasheet for LCD Display LCM1602C](https://www.waveshare.com/datasheet/LCD_en_PDF/LCD1602.pdf)| LCD Display |
|[Tutorial for operating LCD Display with Arduino](http://henrysbench.capnfatz.com/henrys-bench/arduino-displays/ywrobot-lcm1602-iic-v1-lcd-arduino-tutorial/)| LCD Display |
|[About initializing LCD displays](http://web.alfredstate.edu/faculty/weimandn/lcd/lcd_initialization/lcd_initialization_index.html)| LCD Display |
|[Datasheet for microprocessor ATmega328P](http://ww1.microchip.com/downloads/en/DeviceDoc/Atmel-7810-Automotive-Microcontrollers-ATmega328P_Datasheet.pdf)| Basics |
|[Arduino board Reference Design](https://www.arduino.cc/en/uploads/Main/arduino-uno-schematic.pdf)| Basics |
|[Arduino Libraries for reference](https://github.com/arduino/ArduinoCore-avr/tree/master/libraries)| SPI, Serial, etc|

## Building and flashing commands

Use these commands to build and flash the programm on the board. <BR>
These commands assume that everything is located in "main.c".


*  `avr-gcc -Os -DF_CPU=16000000UL -mmcu=atmega328p -c -o main.o main.c && avr-gcc -mmcu=atmega328p main.o -o main && avr-objcopy -O ihex -R .eeprom main main.hex`
*  `sudo avrdude -F -V -c arduino -p ATMEGA328P -P /dev/ttyACM0 -b 115200 -U flash:w:main.hex`



