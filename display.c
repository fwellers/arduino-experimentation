#include <util/delay.h>
#include <avr/io.h>

#include "display.h"

static uint8_t payload;

static uint8_t portindex[] = {D0, D1, D2, D3, D4, D5, D6, D7};

/**
 * Initialize all necessary ports as output.
 **/
void init()
{
	DDRD |= _BV(PORTD4) | _BV(PORTD5) | _BV(PORTD6) | _BV(PORTD7);
	DDRB |= _BV(PORTB0) | _BV(PORTB1) | _BV(PORTB2) | _BV(PORTB3) | _BV(PORTB4) | _BV(PORTB5);
}

int set_pin(uint8_t value, uint8_t pin, int port)
{
	if(value)
	{
		if (port < 0) {
			PORTB |= _BV(pin);
			return 0;
		} else {
			PORTD |= _BV(pin);
			return 0;
		}
	} else {
		if (port < 0) {
			PORTB &= ~_BV(pin);
			return 0;
		} else {
			PORTD &= ~_BV(pin);
			return 0;
		}
	}
	return 1;
}

void transfer_payload(uint8_t inputtype, uint8_t transfermode)
{
	set_pin(inputtype, RS, 1);
	set_pin(1, E, 1);
	if (transfermode == MODE_8BIT) {
		uint8_t mask = 0x01;
		for (int i = 0; i < 8; i++)
		{
			set_pin((payload & mask), portindex[i], ((i < 2) ? 1 : -1));
			mask = mask << 1;
		}
		_delay_ms(1);
		set_pin(0, E, 1);
		_delay_ms(1);
	} else if (transfermode == MODE_4BIT) {
		uint8_t mask = 0x01;
		for (int i = 0; i < 4; i++) {
			set_pin((payload & mask), portindex[i], ((i < 2) ? 1 : -1));
			mask = mask << 1;
		}
		_delay_ms(1);
		set_pin(0,E,1);
		_delay_ms(1);
		set_pin(1, E, 1);
		for (int i = 0; i < 4; i++) {
			set_pin((payload & mask), portindex[i], ((i < 2) ? 1 : -1));
			mask = mask << 1;
		}
		_delay_ms(1);
		set_pin(0, E, 1);
		_delay_ms(1);
	}
	
	set_pin(inputtype, RS, 0);
}

void start_display()
{
	payload = OPCODE_CLEAR;
	transfer_payload(INPUT_INSTRUCTION, MODE_8BIT);
	_delay_ms(1);
	
	payload = OPCODE_FUNCTION_SET;
	transfer_payload(INPUT_INSTRUCTION, MODE_8BIT);
	_delay_ms(1);

	payload = OPCODE_ON;
	transfer_payload(INPUT_INSTRUCTION, MODE_8BIT);
	_delay_ms(1);

	payload = OPCODE_ENTRY_MODE_SET;
	transfer_payload(INPUT_INSTRUCTION, MODE_8BIT);
	_delay_ms(1);

}

int main (void)
{
	init();
	start_display();
	while (1)
	{
		payload = 0xff;
		transfer_payload(INPUT_DATA,  MODE_8BIT);
		_delay_ms(1000);
		
		payload = 0x41;
		transfer_payload(INPUT_DATA, MODE_8BIT);
		_delay_ms(1000);
	}
}
