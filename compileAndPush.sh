#!/bin/bash

avr-gcc -Os -DF_CPU=16000000UL -mmcu=atmega328p -c -o display.o display.c && avr-gcc -mmcu=atmega328p display.o -o display && avr-objcopy -O ihex -R .eeprom display display.hex && sudo avrdude -F -V -c arduino -p ATMEGA328P -P /dev/ttyACM0 -b 115200 -U flash:w:display.hex
